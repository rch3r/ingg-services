def call(Map<String, String> config = [:]) {
    echo "ingg-services"

    echo "Deploying ${config.branch}"

    def globalProperties = [:]

    node {
        stage("clean") {
            cleanWs()
        }

        stage("compose") {
            checkout scm

            if (fileExists("global.properties")) {
                globalProperties = readProperties file: 'global.properties'
            }

            eachDeployment(globalProperties) { property ->
                echo "Key ${property.key}: ${property.value}"

//                withEnv(["DOCKER_HOST=ssh://root@${property.value}"]) {
//                    sh "docker stack deploy -c compose.yaml ingg --with-registry-auth"
//                }

//                docker.withServer("ssh://root@${property.value}") {
//                    sh "docker stack deploy -c compose.yaml ingg --with-registry-auth"
//                }

                docker.withServer("tcp://${property.value}:2375") {
                    sh "docker stack deploy -c compose.yaml ingg --with-registry-auth"
                }
            }
        }
    }
}

def eachDeployment(Map<String, String> config, closure) {
    config.each { entry ->
        closure(entry)
    }
}
